# Matrix Network Front-End

- Module 4 Social Network Project
- Created by: **Asen Yordanov** & **Toma Bonev**

Trello Link: [Trello](https://trello.com/b/gk9qhUMq/matrix)
Back-End Link: [Back-End](https://gitlab.com/tomabonev/matrix-network)

**Welcome to the Matrix, a social network web application for connecting people and allowing them to create,comment and join different activities.**

![Image](https://res.cloudinary.com/dpc0sub89/image/upload/v1606311443/Matrix.Website.ReadMe.Photos/loginPage_ymzujx.jpg)


# Technologies
***

- ASP .NET Core
- ASP .NET Identity
- Entity Framework Core
- MS SQL Server
- SQL
- JWT Token
- SignalR
- React
- TypeScript
- Semantic UI
- Azure
- HTML 5
- CSS 3


# Description
***
The mission of the Matrix is to allow users to sign up to events (similar to MeetUp or Facebook). 
Apart from that, every user should be able to:

```
1.	Create Activity
2.	Attend/Cancel attandance of an activity
3.	Comment an activity
4.	Follow/Unfollow another user
5.	Upload and Crop an image to the cloud
6.	Sort/Filter between future and past activities
7.	Edit Profile
```
Matrix is a community driven web app and every member can create, comment and attend an activity and also follow another matrix member.

# Areas
***
- Public - accessible without authentication
- Private - available after registration

# Public Part
***

The public part of our application should be accessible without authentication. This includes the application start page, the user login and user registration forms.
People that are not authenticated cannot see any user-specific activities, they can only fill up the registration form or log in.  

- **Login** : The newly created user can log in to our app by using email and password.
- **Registration** : It requires filling the more information about the upcoming user: username, display name, email and password.

---
# Private Part
***

Not everyone can join the Matrix! Only those who have been chosen and have taken the blue pill :)
People that are not authenticated cannot see any user-specific details, nor activities.

- **All Activities** : When the user has successfully logged in, he will be able to see all the upcoming activities within the Matrix.
- **Filter & Sort by Date** : Every visitor is able to filter activity by "I'm Going" and "I'm Hosting" in the sidebar or use the sort date calendar for each activity.

![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/Activities_p0uzh0.jpg)
---
 - **Create Activity** : A create activity functionality exist with the required fields (ex. username, display name, email, password).

![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/CreateActivity_scyizr.jpg)
---
 - **View Activity** : When selecting a certain "Activity" the visitor will be able to see more information about it and eventually join in.
 - **Comment Activity** : Each user has the capability to comment an activity in Real-time :)
 - **SideBar Status** : Once we join an activity, our image and name will appear in the side bar.

![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/Activity_wz1dec.jpg)
---
 - **User Profile** : By selecting the name of any user, we would be redirected to his/her profile page.
 - **Follow/Unfollow** : Each user has the posibility to follow or unfollow another Matrix user.

![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/Photos_x6225k.jpg)
---
- **Upload and Crop a Photo** : Each user has the posibility to upload a picture to the cloud and also crop it :)
![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/Upload_Image_nixfuj.jpg)
---
 - **List of Following** : By selecting the following tab in our sidebar, we would get a list of the people we follow.
 - **List of Followers** : By selecting the follower's tab in our sidebar, we would get a list of the people are following us.

 ![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/Followers_intxx2.jpg)
---
- **Past,Future and Hosting Activities** : Under each profile we have a list of all past,future and hosting events.
 ![Important](https://res.cloudinary.com/dpc0sub89/image/upload/v1606305560/Matrix.Website.ReadMe.Photos/UserActivities_r6023b.jpg)
---
