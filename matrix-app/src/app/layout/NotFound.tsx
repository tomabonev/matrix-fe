import React from "react";
import { Link} from "react-router-dom";
import { Button, Segment, Image } from "semantic-ui-react";

const NotFound = () => {
  return (
    <Segment placeholder>
      <Image src={"/assets/pageNotFound1.png"} style={{ marginBottom: 18 }} />     
      <Image
        size="tiny"
        src="/assets/logo.png"
        alt="logo"
        style={{ marginBottom: 18 }}
        centered
      />
      <Segment.Inline>
        <Button as={Link} to="/activities" color="olive">
          Return to the Matrix page
        </Button>
      </Segment.Inline>
    </Segment>
  );
};

export default NotFound;
